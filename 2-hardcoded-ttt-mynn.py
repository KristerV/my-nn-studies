import MyNN
import numpy as np

game_start = np.array([
    [0,0,1,0,0,1,0,0,0],
    [1,1,0,0,0,0,0,0,0],
    [0,0,1,0,1,0,0,0,0],
    [0,0,0,0,1,0,0,1,0],
])

winning_state = np.array([
    [0,0,0,0,0,0,1,0,0],
    [0,0,1,0,0,0,0,0,0],
    [0,0,0,0,0,0,1,0,0],
    [0,1,0,0,0,0,0,0,0],
])

nn = MyNN.new(game_start,winning_state)

for i in range(1500):
    nn.feedforward()
    nn.backprop()

print(nn.output)

import torch
import torch.nn as nn
from torch.utils.data import TensorDataset
from torch.utils.data import DataLoader
import torch.nn.functional as F
import numpy as np
from random import randrange
import copy


# Game global variables
game_state_start = [
    [0., 0, 0],
    [0, 0, 0],
    [0, 0, 0],
]
game_state = [
    [0., 0, 0],
    [0, 0, 0],
    [0, 0, 0],
]
game_state_last = []
game_won = False
wins = [0, 0]  # AI / RandBot

# How to train
total_games = 1000
current_game = 0
train_after_games = total_games / 5
learning_rate = 1e-1

# Model
inputs = []
targets = []
model = nn.Linear(9, 2)
loss_fn = F.mse_loss
opt = torch.optim.SGD(model.parameters(), lr=learning_rate)


def train():
    # PyTorch training algo
    inputs_t = torch.tensor(inputs, dtype=torch.float)
    targets_t = torch.tensor(targets, dtype=torch.float)
    train_ds = TensorDataset(inputs_t, targets_t)
    train_dl = DataLoader(train_ds, 50, shuffle=True)
    for epoch in range(100):
        for xb, yb in train_dl:
            pred = model(xb)
            loss = loss_fn(pred, yb)
            loss.backward()
            opt.step()
            opt.zero_grad()
        if epoch % 10 == 0:
            print("loss", loss)


def game_winner():
    # Check if someone won the game. Who?
    if 0 != game_state[0][0] == game_state[1][1] == game_state[2][2]:
        return game_state[0][0]
    if 0 != game_state[0][2] == game_state[1][1] == game_state[2][0]:
        return game_state[0][2]

    for i in range(3):
        if 0 != game_state[0][i] == game_state[1][i] == game_state[2][i]:
            return game_state[0][i]
        if 0 != game_state[i][0] == game_state[i][1] == game_state[i][2]:
            return game_state[i][0]

    return False


def game_full():
    # Is the game board full?
    for i in range(3):
        for j in range(3):
            if game_state[i][j] == 0:
                return False
    return True


def game_move(row, col, player):
    # Player makes a move
    global game_won
    global game_state
    global game_state_last

    if game_state[row][col] == 0:
        game_state_last = copy.deepcopy(game_state)
        game_state[row][col] = player
    win = game_winner()
    game_print_state()
    if (win):
        wins[win-1] += 1
        print("---------------------------- GAMES",
              current_game, "/", total_games, "--- WINS", wins, "--- RATIO", round(wins[0] / (wins[1]+1e-10), 2))
        game_won = True
        return win
    else:
        return False


def flatten(l):
    # Reduce array dimension
    return [float(y) for x in l for y in x]


def flatten_deep(l):
    # Reduce array dimension, deep one level
    return [flatten(x) for x in l]


def game_print_state():
    # Print game board in a readable way
    for row in game_state:
        print(row)
    print('')


def game_reset_maybe():
    # If the game needs resetting, do it.
    global game_state
    global game_won
    global game_state_last
    global current_game
    if (game_won or game_full()):
        current_game += 1
        game_state = copy.deepcopy(game_state_start)
        game_won = False
        game_state_last = []
        if current_game % train_after_games == 0:
            train()


# Game loop.
# Play until current_game > total_games
# AI player is number 1, RandBot is 2.
# AI player learns regardless of win or lose.
while True:
    # Rand player makes a move
    print("Rand Player")
    rand_row = 0
    rand_col = 0
    # Pick a spot randomly until an empty spot foud.
    while True:
        rand_row = randrange(3)
        rand_col = randrange(3)
        if game_state[rand_row][rand_col] == 0:
            break
    # Make the move
    winner = game_move(rand_row, rand_col, 2)
    # If RandBot won game, let AI learn
    if winner == 2:
        inputs.append(flatten(game_state_last))
        targets.append([float(rand_row), float(rand_col)])
        switched_table = [max(0, x - 1) for x in flatten(game_state_last)]
        inputs.append(switched_table)
        targets.append([float(rand_row), float(rand_col)])

    # Does the board need to be reset or game ended?
    game_reset_maybe()
    if current_game > total_games:
        break

    # AI makes a move
    # Figure out the best move
    pred = model(torch.tensor(flatten(game_state))).detach().numpy()
    print("AI Player prediction", pred)
    row = min(abs(int(round(pred[0]))), 2)
    col = min(abs(int(round(pred[1]))), 2)
    # If move is not empty, pick at random
    while game_state[row][col] != 0:
        row = randrange(3)
        col = randrange(3)
    # Make the move
    winner = game_move(row, col, 1)
    # Did AI win? Learn from it.
    if winner == 1:
        inputs.append(flatten(game_state_last))
        targets.append([float(row), float(col)])

    # Does the board need to be reset or game ended?
    game_reset_maybe()
    if current_game > total_games:
        break


print(" =============== GAME OVER =============== ")
print("             AI [", wins[0], "/", wins[1], "] RandBot")
print("               ratio:", round(wins[0] / (wins[1]+1e-10), 2))

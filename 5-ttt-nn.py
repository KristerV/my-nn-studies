import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import TensorDataset
from torch.utils.data import DataLoader
from torch.utils.data import random_split
import numpy as np
from random import randrange
import copy
import time
start_time = time.time()

# Training variables
batch_size = 512
learning_rate = 2e-1
total_games = 10000
train_after_games = int(total_games / 5 + 1)
epochs_per_training = 200

# Model
inputs = []
targets = []


class TTTModel(nn.Module):
    """Feedfoward neural network with 1 hidden layer"""

    def __init__(self, nnShape):
        super().__init__()
        self.layers = []
        for i in range(1, len(nnShape)):
            self.layers.append(nn.Linear(nnShape[i-1], nnShape[i]))
        self.layers = nn.ModuleList(self.layers)

    def forward(self, xb):
        # Flatten the game board
        size = 1 if xb.size(0) == 3 else xb.size(0)
        out = xb.view(size, -1)
        for layer in self.layers:
            out = layer(out)
            out = F.relu(out)
        return out

    def training_step(self, batch):
        boards, labels = batch
        out = self(boards)                   # Generate predictions
        loss = F.cross_entropy(out, labels)  # Calculate loss
        return loss

    def validation_step(self, batch):
        images, labels = batch
        out = self(images)                    # Generate predictions
        loss = F.cross_entropy(out, labels)   # Calculate loss
        acc = accuracy(out, labels)           # Calculate accuracy
        return {'val_loss': loss, 'val_acc': acc}

    def validation_epoch_end(self, outputs):
        batch_losses = [x['val_loss'] for x in outputs]
        epoch_loss = torch.stack(batch_losses).mean()   # Combine losses
        batch_accs = [x['val_acc'] for x in outputs]
        epoch_acc = torch.stack(batch_accs).mean()      # Combine accuracies
        return {'val_loss': epoch_loss.item(), 'val_acc': epoch_acc.item()}

    def epoch_end(self, epoch, result):
        print("Epoch [{}], val_loss: {:.4f}, val_acc: {:.4f}".format(
            epoch, result['val_loss'], result['val_acc']))


dynamic_lr = learning_rate


def fit(model, opt_func=torch.optim.SGD):
    global dynamic_lr
    inputs_t = torch.tensor(inputs, dtype=torch.float)
    targets_t = torch.tensor(targets, dtype=torch.long)
    train_ds = TensorDataset(inputs_t, targets_t)
    train_dl = DataLoader(train_ds, batch_size, shuffle=True)

    print("dynamic_lr", dynamic_lr)
    optimizer = opt_func(model.parameters(), learning_rate)
    for epoch in range(epochs_per_training):
        loss = None
        for batch in train_dl:
            loss = model.training_step(batch)
            loss.backward()
            optimizer.step()
            optimizer.zero_grad()
        if epoch % 10 == 0:
            print("loss", loss)
    dynamic_lr = 1e-3


def flatten(l):
    # Reduce array dimension
    return [float(y) for x in l for y in x]


def flatten_deep(l):
    # Reduce array dimension, deep one level
    return [flatten(x) for x in l]


class Game():

    def __init__(self, model):
        self.model = model
        self.games_count = -1
        self.wins = [0, 0]
        self.players_turn = 1
        self.reset_game_state()
        self.game_loop()

    def reset_game_state(self):
        self.games_count += 1
        self.game_state = [
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
        ]
        self.game_state_last = None

    def get_winner(self):

        if 0 != self.game_state[0][0] == self.game_state[1][1] == self.game_state[2][2]:
            return self.game_state[0][0]
        if 0 != self.game_state[0][2] == self.game_state[1][1] == self.game_state[2][0]:
            return self.game_state[0][2]

        for i in range(3):
            if 0 != self.game_state[0][i] == self.game_state[1][i] == self.game_state[2][i]:
                return self.game_state[0][i]
            if 0 != self.game_state[i][0] == self.game_state[i][1] == self.game_state[i][2]:
                return self.game_state[i][0]

        return None

    def is_board_full(self):
        for i in range(3):
            for j in range(3):
                if self.game_state[i][j] == 0:
                    return False
        return True

    def make_move(self, player, row, col):
        if self.game_state[row][col] == 0:
            self.game_state_last = copy.deepcopy(self.game_state)
            self.game_state[row][col] = player

    def print_state(self, bold_row, bold_col):
        for i in range(3):
            for j in range(3):
                if i == bold_row and j == bold_col:
                    print(' ' + Color.GREEN +
                          str(self.game_state[i][j]) + Color.END, end='')
                else:
                    print(' ' + str(self.game_state[i][j]), end='')
            print('')
        print('')

    def game_loop(self):
        while self.games_count < total_games:
            row, col = self.get_rand_move() if self.players_turn == 2 else self.get_ai_move()
            self.make_move(self.players_turn, row, col)
            self.print_state(row, col)
            winner = self.get_winner()

            if winner == 1:  # AI player
                self.wins[0] += 1
                self.save_training_data(
                    flatten(self.game_state_last), row, col)
            elif winner == 2:  # RandBot
                self.wins[1] += 1
                # Learn to block opponents move
                self.save_training_data(
                    flatten(self.game_state_last), row, col)
                # Learn to make same move opponent did
                switched_table = [
                    1 if x == 2 else 2 if x == 1 else 0 for x in flatten(self.game_state_last)]
                self.save_training_data(switched_table, row, col)

            if self.is_board_full() or winner != None:
                print("Winner:", winner, "\n")
                self.reset_game_state()
                print(" --- GAMES:", self.games_count, "/", total_games, "--- WINS:",
                      self.wins, "--- RATIO:", round(self.wins[0] / (self.wins[1]+1e-10), 2))
                if self.games_count > 0 and self.games_count % train_after_games == 0:
                    self.wins = [0, 0]
                    fit(self.model)

            self.players_turn = 1 if self.players_turn == 2 else 2

        print("\n  - - - - - - - - - - - GAME OVER - - - - - - - - - - - \n")
        print("Execution time:", int(time.time() - start_time), "sec")

    def save_training_data(self, board, row, col):
        inputs.append(board)
        targets.append(row * 3 + col)

    def get_rand_move(self):
        while True:
            row = randrange(3)
            col = randrange(3)
            if self.game_state[row][col] == 0:
                print("RandBot:", row, col)
                return [row, col]

    def get_ai_move(self):
        out = self.model.forward(torch.tensor(
            self.game_state, dtype=torch.float))
        max_prob, pred = torch.max(out, dim=-1)
        pred = pred.item()
        row = int(pred / 3)
        col = pred - (3 * row)
        print(
            f"AI pred: [{row}, {col}]. Prob: {round(max_prob.item(), 3)}.")
        while self.game_state[row][col] != 0:
            row = randrange(3)
            col = randrange(3)
        return [row, col]


class Color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'


Game(TTTModel([9, 64, 64, 64, 9]))

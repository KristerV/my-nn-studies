import numpy as np
import pprint
pp = pprint.PrettyPrinter(indent=4)

class MyNN:
    def __init__(self, nn_shape, inp, expected):
        self.input = np.array(inp)
        self.expected = np.array(expected)
        params = []
        for i in range(1, len(nn_shape)):
            params.append({
                "weights": np.random.randn(nn_shape[i], nn_shape[i-1]),
                # "biases": np.zeros((nn_shape[i], 1)) # Later
            })
            print(params[-1]["weights"].shape)
        self.params = params
        self.outputs = list(range(len(nn_shape)))

    def sigmoid(self, x):
        return 1.0/(1+ np.exp(-x))

    def sigmoid_derivative(self, x):
        return x * (1.0 - x)

    def forward_prop(self):
        prev_layer = self.input
        for i in range(len(self.params)):
            self.outputs[i] = self.sigmoid(np.dot(prev_layer, self.params[i]["weights"]))

    def back_prop(self):
        for i in reversed(range(len(self.params))):
            print("i", i)

            output = self.outputs[i+1]
            cost = (self.expected - output)*self.sigmoid_derivative(output)
            prev_weights = self.params[i-1]["weights"] if i > 0 else self.input.T

            # derivative = np.dot(self.outputs[i-1].T, (2*(self.expected - output) * self.sigmoid_derivative(output)))

            print("-------------")
            print(self.params[i]["weights"].shape)
            print(prev_weights.shape)
            print(cost.shape)
            pp.pprint(self.params[i]["weights"])
            pp.pprint(prev_weights)
            pp.pprint(cost)

            self.params[i]["weights"] += np.dot(prev_weights, cost)


inp = [
    [0,0,1],
    [0,1,1],
    [1,0,1],
    [1,1,1]
]
expected = [[0],[1],[1],[0]]

mynn = MyNN([4,3,3], inp, expected)
pp.pprint(mynn.params)

for i in range(1):
    mynn.forward_prop()
    mynn.back_prop()
pp.pprint(mynn.outputs)
